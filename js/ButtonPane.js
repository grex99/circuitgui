class ButtonPane{
    selected;
    buttontree;
    constructor(){
        this.buttontree = new ItemTree();
    }
    createButtonFromName(values){
        for(let idx in values){
            let butt;
            if(values[idx].length == 0){
                butt = new ItemButton("empty");
            }else{
                butt = new ItemButton(values[idx]);
            }
            this.buttontree.add("root", butt);
        }
        this.updateButtonPane();
    }
    updateButtonPane(){
        let pane = document.getElementById("left");
        while(pane.firstChild){
            pane.removeChild(pane.lastChild);
        }
        
        let buttons = this.buttontree.getChildren("root");
        let c = 0;
        let i = 0;
        while(i < buttons.length){
            pane.appendChild(buttons[i].canvas);
            let children = this.buttontree.getNodeFromValue(buttons[i]).children;
            i++;
            c++;
            if(children.length >0){
                while(c % 3 != 0){
                    if(buttons[i] != undefined){
                        pane.appendChild(buttons[i].canvas);
                    }else{
                        pane.appendChild(new ItemButton("empty").canvas);
                    }
                    i++;
                    c++;
                }
                for(let idx in children){
                    pane.appendChild(children[idx].value.canvas);
                    c++;
                }
                while(c % 3 != 0){
                    pane.appendChild(new ItemButton("empty").canvas);
                    c++;
                }
            }
        }
    }
    redrawButtons(){
        let iter = new ValueIterator(this.buttontree);
        while(iter.hasNext()){
            let b = iter.getNext();
            ButtonPane.drawItem(b.canvas);
            if(this.selected == b && itemtree.getSiblings(b.canvas.item.name).length >0 && b.canvas.showArrow == false){
                b.canvas.showArrow = true;
                b.canvas.drawArrowButton();
            }
        }
        if(this.selected != undefined){
            ButtonPane.drawItem(this.selected.canvas, "#0080cf",2);
        }
    }
    expandButton(button){
        let siblings = itemtree.getSiblings(button.name);
        for(let i in siblings){
            let butt = new ItemButton(siblings[i]);
            butt.canvas.showArrow = false;
            this.buttontree.add(button, butt);
        }
        this.updateButtonPane();
    }
    collapseButtons(){
        let iter = new NodeIterator(this.buttontree);
        while(iter.hasNext()){
            let buttonNode = iter.getNext();
            buttonNode.children = [];
        }
        this.updateButtonPane();
    }
    selectButton(butt){
        this.selected = butt;
        let node = this.buttontree.getNodeFromValue(butt);
        node.parent.value = butt;
        this.collapseButtons();
    }
    static drawItem(canvas, color = "#000000", shift = 0){
        if(canvas == undefined) return;
        let gc_save = gc;
        gc = drawPane.getContext(canvas.getContext("2d"));
        gc.clearRect(0, 0, canvas.width, canvas.height);
        gc.save();
        gc.translate(canvas.width/2,canvas.height/2-shift);
        gc.scale(0.6, 0.6);
        if(canvas.item.type.search("bipoles") >= 0){
            canvas.item.p1 = new Vector(-75,0);
            canvas.item.p2 = new Vector(75,0);
        }
        canvas.item.draw(color);
        gc.restore();
        gc = gc_save;
    }
}

class ItemButton{
    canvas;
    name;
    constructor(name){
        this.name = name;
        this.canvas = document.createElement("CANVAS")
        this.canvas.width = document.documentElement.clientWidth/15.1;
        this.canvas.height = 60;
        this.canvas.item = new Item(name);
        this.canvas.showArrow = itemtree.getSiblings(name).length>0;
        this.canvas.arrowDown = true;

        this.canvas.button = this;
        this.canvas.className = "itemButt";
        
        this.canvas.onmousedown = this.onmousedown;
        this.canvas.onmousemove = this.onmousemove;
        this.canvas.onmouseleave = this.onmouseleave;
        this.canvas.onmouseenter = this.onmouseenter;
        this.canvas.drawArrowButton = this.drawArrowButton;

        ButtonPane.drawItem(this.canvas);

        if(name == "empty"){
            this.canvas.drawArrowButton = function(){};
        }
    }
    onmousedown(e){
        let pos = new Vector(e.pageX-this.offsetLeft, e.pageY-this.offsetTop);
        if(pos.x > this.width/6*3.8 && pos.y > this.height/6*3.8){
            if(this.arrowDown){
                buttonpane.expandButton(this.button);
            }else{
                buttonpane.collapseButtons(this.button);
            }
            

            this.arrowDown = !this.arrowDown;
            this.drawArrowButton("#0080cf");
        }
        else{
            drawPane.canvas.nextItem = new Item(this.item.name);
            PlaceTool.apply();
            buttonpane.selectButton(this.button);
            buttonpane.redrawButtons();
            this.drawArrowButton();
        }
    }
    onmousemove(e){
        let pos = new Vector(e.pageX-this.offsetLeft, e.pageY-this.offsetTop);
        let g = this.getContext("2d");

        g.clearRect(0,0,this.width, this.height);
        buttonpane.redrawButtons();
        
        if(buttonpane.selected == this.button){
            ButtonPane.drawItem(this, "#0080cf", 2);
        }else{
            ButtonPane.drawItem(this, [], 2);
        }
        
        
        if(pos.x > this.width/6*3.8 && pos.y > this.height/6*3.8){
            this.drawArrowButton("#0080cf")
        }
        else{
            this.drawArrowButton();
        }
    }
    onmouseleave(e){
        buttonpane.redrawButtons();
    }
    onmouseenter(e){
        this.onmousemove(e);
    }
    drawArrowButton(color = "#000000"){
        let g = drawPane.getContext(this.getContext("2d"));
        
        g.save()
        g.strokeStyle = color;
        g.lineWidth = 1;

        let x = this.width/6*4.2;
        let y = this.height/6*4.5;
        let w = this.width/5;
        let h = this.height/5;
        let r = 4.5;

        g.clearRect(x,y,w,h)
        if(this.showArrow == false){
            g.save();
            return;
        }
        g.roundRect(x, y, w, h, r);
        g.stroke();
        
        g.beginPath();
        if(this.arrowDown){
            g.moveTo(x+w*0.25, y+h*0.25);
            g.lineTo(x+w/2,    y+h*0.75);
            g.lineTo(x+w*0.75, y+h*0.25);
        }else{
            g.moveTo(x+w*0.25, y+h*0.75);
            g.lineTo(x+w/2,    y+h*0.25);
            g.lineTo(x+w*0.75, y+h*0.75);
        }
        g.stroke();

        g.restore();
    }
}