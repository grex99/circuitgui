class TextPane{
    textarea = document.getElementById("textarea");
    topline = "\\begin{figure}[!h]\n\\begin{center}\\begin{circuitikz}\\draw\n";
    endline = ";\\end{circuitikz}\\end{center}\n\\end{figure} ";
    itemHandler;
    constructor(){
        this.textarea.value = this.topline + this.endline;
        this.itemHandler = drawPane.canvas.itemHandler;
    }
    update(){
        this.textarea.value = this.topline;
        this.textarea.value += this.itemHandler.getLatex();
        this.textarea.value += this.endline;
    }
}