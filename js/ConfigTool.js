class ConfigTool{
    static onmousedown(e) {
        let pos = new Vector(e.pageX-this.offsetLeft, e.pageY-this.offsetTop);
        let item = drawPane.canvas.itemHandler.getItemAtPos(pos);
        if(item != undefined && item.type.search("bipole") > 0){
            modal.newModal(item);
        }
    }
    static onmousemove(e){
        let pos = new Vector(e.pageX-this.offsetLeft, e.pageY-this.offsetTop);
        drawPane.update();
        let item = drawPane.canvas.itemHandler.getItemAtPos(pos);
        if(item != undefined && item.type.search("bipole") > 0){
            item.draw("#0000FF");
        }
    }
}

let configToolButton = document.getElementById("configToolButt");
configToolButton.addEventListener("click", (e) => {
    drawPane.onmousemove(ConfigTool.onmousemove);
    drawPane.onmousedown(ConfigTool.onmousedown);
})

