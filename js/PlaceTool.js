class PlaceTool{
    /**
     * snaps the values from a vector to the grid
     * @param {mousedown} e 
     */
    static onmousedown(e){
        let pos = new Vector(e.pageX-this.offsetLeft, e.pageY-this.offsetTop);
        if(window.TouchEvent && e instanceof TouchEvent ){
            pos = new Vector(e.changedTouches[0].pageX-this.offsetLeft, e.changedTouches[0].pageY-this.offsetTop);
        }
        this.grid.snap(pos);
        this.itemHandler.snap(pos);
        
        if(this.nextItem.type.search("bipole") >= 0){
            if(this.nextItem.p1 == undefined){
                this.nextItem.p1 = new Vector(0,0);
                this.nextItem.center = pos.copy();
                drawPane.canvas.itemHandler.checkForPrevious(this.nextItem, pos);
            } 
            else if(pos.equals(this.nextItem.p1abs())){
                this.nextItem = new Item(this.nextItem.name, pos);
                if(this.itemHandler.getLast() != undefined){
                    this.itemHandler.getLast().following = undefined;
                }
            } 
            else{
                PlaceTool.mouseMove(this.nextItem, pos);

                if(drawPane.canvas.itemHandler.checkForFollowing(this.nextItem, pos)){
                    this.itemHandler.add(this.nextItem);
                    this.nextItem = new Item(this.nextItem.name, pos);
                }else{
                    this.itemHandler.add(this.nextItem);
                    this.nextItem = new Item(this.nextItem.name, pos);
                    this.nextItem.p1 = new Vector(0,0);

                    this.itemHandler.getLast().following = this.nextItem;
                    this.nextItem.previous = this.itemHandler.getLast();
                }
            } 
        }else if(this.nextItem.type.search("monopole")>=0){
            PlaceTool.mouseMove(this.nextItem, pos);
            this.itemHandler.add(this.nextItem);
            this.nextItem = new Item(this.nextItem.name, pos);
        }     
        this.update();
        textpane.update();   
    }

    /**
     * snaps the values from a vector to the grid
     * @param {mousemove} e
     */
    static onmousemove(e){
        let pos = new Vector(e.pageX-this.offsetLeft, e.pageY-this.offsetTop);
        if(window.TouchEvent && e instanceof TouchEvent ){
            pos = new Vector(e.changedTouches[0].pageX-this.offsetLeft, e.changedTouches[0].pageY-this.offsetTop);
        }
        this.grid.snap(pos);
        this.itemHandler.snap(pos);

        if(this.nextItem.type.search("bipole") >= 0){
            if(this.nextItem.p1 != undefined){
                
                this.update();
    
                if(PlaceTool.mouseMove(this.nextItem,pos)){
                    this.nextItem.draw();
                }
            }
        }
        else if(this.nextItem.type.search("monopole") >= 0){
            PlaceTool.mouseMove(this.nextItem, pos);
            this.update();
            this.nextItem.draw();
        }
    }

    /**
     * set a mousemouve for this Item
     * @param {Vector} pos  
     */
    static mouseMove(item, pos){
        if(item.type.search("bipole") >= 0){
            if(item.p1==undefined && item.p2 == undefined){
                item.center = pos.copy();
                return true;
            }else if(item.p1!=undefined && item.p2 == undefined){
                let shift = Vector.sub(pos,item.center)
                item.center = shift.copy().mult(0.5).add(item.center);
                item.p1 = shift.copy().mult(-0.5);
                item.p2 = Vector.sub(pos, item.center);

                item.rotate(Vector.sub(pos,item.center).angle())
                return true;
            }else if(!item.p1abs().equals(pos)){
                let angle = item.p2.angle();
                if(Vector.sub(pos, item.center).abs() > 5){
                    let shift = Vector.sub(pos, Vector.add(item.center,item.p2));
                    item.center.add(shift.copy().mult(0.5));

                    item.p2 = Vector.sub(pos, item.center);
                    item.p1 = item.p2.copy().mult(-1);
                }
                item.rotate(item.p2.angle()-angle);
                return true;
            }else{
                return false;
            }

        }
        else if(item.type.search("monopole")>=0){
            item.center = pos.copy();
            return true;
        } 
    }
    static apply(){
        drawPane.onmousemove(PlaceTool.onmousemove);
        drawPane.onmousedown(PlaceTool.onmousedown);
    }
}


let placeToolButton = document.getElementById("placeToolButt");
placeToolButton.addEventListener("click", (e) => {
    PlaceTool.apply();
})