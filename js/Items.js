class Item{
    name = "";
    type = "";
    p1;
    p2;
    center;
    lines = [];
    arcs = [];
    curves = [];
    pins = [];
    pinDirections = [];
    previous = undefined;
    following = undefined;
    constructor(name, center_ = new Vector(0,0)){
        this.name = name;
        this.center = center_;
        this.type = itemtree.getPath(name);
        if(this.type==undefined) this.type = "";
        if(this.type.search("monopoles")>= 0) this.pins = [new Vector(0,0)];
        

        if(name == "short"){
            this.pins = Vector.newArray(1,0, 0,0)    
        }
        else if(name == "european resistor"){
            this.lines = [new Vector(-40,-15), Vector.newArray(80,0, 0,30, -80,0, 0,-30)];
            this.pins = Vector.newArray(-40,0, 40,0);
        }
        else if(name == "capacitor"){
            this.lines = [new Vector(-10,-30), Vector.newArray(0,60), new Vector(10,-30), Vector.newArray(0,60) ];
            this.pins = Vector.newArray(-10,0, 10,0);
        }
        else if(name == "ecapacitor"){
            this.lines = [new Vector(-10,-25), Vector.newArray(0,50, 6,0, 0,-50, -6,0), new Set(10,-25, "fill=#000000"), Vector.newArray(0,50, -6,0, 0,-50, 6,0), new Vector(-12,-15), Vector.newArray(-10,0), new Vector(-17,-10), Vector.newArray(0,-10)];
            this.pins = Vector.newArray(-10,0, 10,0);
       }
        else if(name == "stroke diode"){
            this.lines = [new Vector(-20,-25), Vector.newArray(40,25, -40,25, 0,-50), new Vector(20,-25), Vector.newArray(0,50), new Vector(20,0), Vector.newArray(-40,0)];
            this.pins = Vector.newArray(-20,0, 20,0);
        }
        else if(name == "american inductor"){
            this.curves = [new Vector(-40,0), Vector.newArray(0,-15, 20,-15, 20,0), new Vector(-20,0), Vector.newArray(0,-15, 20,-15, 20,0), new Vector(0,0), Vector.newArray(0,-15, 20,-15, 20,0), new Vector(20,0), Vector.newArray(0,-15, 20,-15, 20,0),];
            this.pins = Vector.newArray(-40,0, 40,0);
        }
        else if(name == "european inductor"){
            this.lines = [new Set(-40,-15, "fill=#000000"), Vector.newArray(80,0, 0,30, -80,0, 0,-30)];
            this.pins = Vector.newArray(-40,0, 40,0);
        }
        else if(name == "european voltage source"){
            this.lines = [new Vector(-30,0), Vector.newArray(60,0)];
            this.arcs = [new Vector(0,0), [30,0,2*Math.PI]];
            this.pins = Vector.newArray(-30,0, 30,0);
        }
        else if(name == "european current source"){
            this.arcs = [new Vector(0,0), [30,0,2*Math.PI]];
            this.pins = Vector.newArray(-30,0, 30,0);
            this.lines = [new Vector(0,30), Vector.newArray(0,-60)];
        }
        else if(name == "esource"){
            this.arcs = [new Vector(0,0), [30,0,2*Math.PI]];
            this.pins = Vector.newArray(-30,0, 30,0);
        }
        //---MONOPOLES---
        else if(name == "ground"){
            this.lines = [new Vector(0,0), Vector.newArray(0,30), new Vector(-15,30), Vector.newArray(30,0), new Vector(-10,35), Vector.newArray(20,0), new Vector(-5,40), Vector.newArray(10,0)];
        }
        else if(name == "tlground"){
            this.lines = [new Vector(-15,0), Vector.newArray(30,0), new Vector(-10,5), Vector.newArray(20,0), new Vector(-5,10), Vector.newArray(10,0)];
        }
        else if(name == "rground"){
            this.lines = [new Vector(0,0), Vector.newArray(0,30), new Vector(-15,30), Vector.newArray(30,0)];
        }
        else if(name == "circ"){
            this.arcs = [new Set(0,0, "fill=#000000"), [4,0,2*Math.PI]];
        }
        else if(name == "ocirc"){
            this.arcs = [new Set(0,0, "fill=#FFFFFF"), [4,0,2*Math.PI]];
        }
    }
    p1abs(){
        let ret = this.center.copy();
        ret.add(this.p1);
        return ret;
    }
    p2abs(){
        return Vector.add(this.center,this.p2);
    }
    draw(color){
        if(color == undefined) gc.strokeStyle = "#000000";
        else gc.strokeStyle = color;

        for(let idx = 0; idx < this.lines.length; idx+=2){
            gc.movePath(this.lines[idx+1], Vector.add( this.lines[idx], this.center));
            if(this.lines[idx].fill == undefined){
                gc.stroke();
            }else{
                if(color == undefined) gc.fillStyle = this.lines[idx].fill;
                else gc.fillStyle = color;
                gc.fill();
                gc.stroke();
            }
        }
        for(let idx = 0; idx<this.curves.length; idx+=2){
            gc.beginPath();
            gc.moveTo(this.center.x+this.curves[idx].x, this.center.y+this.curves[idx].y)
            let c = this.curves[idx+1];
            let p1 = Vector.add(c[0],this.curves[idx], this.center);
            let p2 = Vector.add(c[1],this.curves[idx], this.center);
            let p3 = Vector.add(c[2],this.curves[idx], this.center);
            gc.bezierCurveTo(p1.x,p1.y,  p2.x,p2.y,  p3.x,p3.y);
            gc.stroke();
        }
        for(let idx = 0; idx<this.arcs.length; idx+=2){
            let pos = Vector.add(this.arcs[idx],this.center);
            gc.beginPath();
            gc.arc(pos.x,pos.y, this.arcs[idx+1][0], this.arcs[idx+1][1], this.arcs[idx+1][2]);
            if(this.arcs[idx].fill == undefined){
                gc.stroke();
            }else{
                if(color == undefined) gc.fillStyle = this.arcs[idx].fill;
                else gc.fillStyle = color;
                gc.fill();
                gc.stroke();
            }
        }
        if(this.type.indexOf("bipole") >= 0 && this.p1!=undefined && this.p2!=undefined && this.pins.length>=2){
            gc.connectVectors(this.p1abs(), Vector.add(this.center, this.pins[0]), this.p2abs(), Vector.add(this.center, this.pins[1]))
            //gc.mark(Vector.add(this.pins[0],this.center), "#FF0000");
            //gc.mark(Vector.add(this.pins[1],this.center), "#0000FF");
            //gc.mark(Vector.add(this.lines[0], this.center), "#00FF00");
        }
    }
    /**
     * rotate this Item, phi in rad
     * @param {Double} phi  
     */
    rotate(phi){
        for(let idx = 0; idx<this.lines.length; idx+=2){
            this.lines[idx].rotate(phi);
            Vector.rotateArray(this.lines[idx+1], phi);
        }
        for(let idx in this.pins){
            this.pins[idx].rotate(phi);
        }
        for(let idx = 0;idx<this.curves.length; idx+=2){
            this.curves[idx].rotate(phi);
            Vector.rotateArray(this.curves[idx+1], phi);
        }
    }
    /**
     * rotate this Item phi degrees
     * @param {Double} phi  
     */
    rotateDeg(phi){
        this.rotate(phi*Math.PI/180);
    }
    /**
     * return true if the distance from any part of this item ist closer to arg: point than maxDist
     * @param {Vector} point
     * @returns {Boolean}
     */
    isAtPos(point){
        let maxDist = 10;
        if(this.center.dist(point)<maxDist) return true;

        for(let line = 0; line<this.lines.length; line+=2){
            let pos = this.lines[line].copy().add(this.center);
            let directions = this.lines[line+1];
            for(let idx in directions){
                let vec = Vector.sub(point,pos);
                if(directions[idx].projectedInLength(vec)){
                    let l = directions[idx].projectLength(vec);
                    let projectedPos = directions[idx].copy().setAbs(l);
                    let dist = Vector.sub(vec,projectedPos).abs();
                    if(dist<maxDist) return true;
                }
                pos.add(directions[idx]);
            }
        }
        if(this.type.search("bipole")>= 0){
            let vec = Vector.sub(this.p2abs(),this.p1abs());
            let toPoint = Vector.sub(point, this.p1abs());
            if( vec.projectedInLength( toPoint) ) {
                let projectedPos = vec.copy().setAbs(vec.projectLength(toPoint));
                let dist = Vector.sub(toPoint,projectedPos).abs();
                if(dist<maxDist) return true;
            }
        }
        return false;
    }
    replaceBy(name){
        let item = new Item(name);
        this.lines = item.lines;
        this.arcs = item.arcs;
        this.curves = item.curves;
        this.pins = item.pins;
        this.pinDirections = item.pinDirections;
        this.previous = item.previous;
        this.following = item.following;
    }
}

class Set extends Vector{
    constructor(x,y, ...other){
        super(x, y);
        for(let i in other){
            if(other[i].search("fill") >= 0){
                this.fill = other[i].split("=")[1];
            }
        }
    }
}