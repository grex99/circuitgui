class ItemHandler{
    items = [];

    /**
     * add a new Item
     * @param {Item} item 
     */
    add(item){
        this.items.push(item);
    }
    /**
     * remove an Item
     * @param {Item} item 
     */
    remove(item){
        if(item.following != undefined){
            item.following.previous = undefined;
        }
        if(item.previous != undefined){
            item.previous.following = undefined;
        }
        this.items.splice(this.items.indexOf(item), 1);
        this.rearange();
    }
    rearange(){
        let rearanged = [];
        for(let i in this.items){
            if(!rearanged.includes(this.items[i])){
                let item = this.items[i];
                let anchor = item;
                while(item.previous != undefined){
                    item = item.previous;
                    if(item == anchor){
                        item.previous = undefined;
                        break;
                    }
                }
                while(item != undefined){
                    rearanged.push(item);
                    if(this.items.includes(item.following) && !rearanged.includes(item.following)){
                        item = item.following;
                    }else{
                        item = undefined; 
                    }
                }
            }
        }
        this.items = rearanged;
    }
    /**
     * draw all Items
     */
    draw(){
        for(let idx = 0; idx<this.items.length; idx++){
            this.items[idx].draw();
        }
    }
    /**
     * get the last Item
     */
    getLast(){
        return(this.items[this.items.length-1]);
    }
    snap(pos){
        let maxDist = 10;
        for(let i in this.items){
            if(this.items[i].type.search("bipole") >= 0){
                if (this.items[i].p1abs().dist(pos) < maxDist){
                    pos = this.items[i].p1abs();
                    return pos;
                } else if (this.items[i].p2abs().dist(pos) < maxDist){
                    pos = this.items[i].p2abs();
                    return pos;
                }
            }else{
                for(let idx in this.items[i].pins){
                    let pin_Abs = Vector.add( this.items[i].pins[idx], this.items[i].center);
                    if(pin_Abs.dist(pos) < maxDist){
                        pos.x = pin_Abs.x;
                        pos.y = pin_Abs.y;
                        return pos;
                    }
                }
            }
        }
    }
    checkForPrevious(item, pos){
        for(let i in this.items){
            if(this.items[i].type.search("bipole") >= 0){                
                if(this.items[i].p2abs().equals(pos) && this.items[i].following == undefined){
                    this.items[i].following = item;
                    item.previous = this.items[i];
                    return true;
                }
            }
        }
        return false;
    }
    checkForFollowing(item, pos){
        for(let i in this.items){
            if(this.items[i].type.search("bipole") >= 0){
                if(this.items[i].p1abs().equals(pos) && this.items[i].previous == undefined){
                    this.items[i].previous = item;
                    item.following = this.items[i];
                    return true;
                }
            }
        }
        return false;
    }
    getLatex(){
        let ret = "";
        let grid = drawPane.canvas.grid;
        this.rearange();

        for(let idx = 0; idx<this.items.length; idx++){
            let item = this.items[idx];

            if(item.type.search("bipole") >= 0){
                if(item.previous == undefined){
                    ret += "  " + grid.getAbsPos(item.p1abs());
                }else{
                    ret += " ";
                }
                ret += " to[" + item.name + "] ++";
                ret += grid.getRelPos(Vector.sub(item.p2, item.p1));
            }else if(item.type.search("monopole") >= 0){
                ret += "  " + grid.getAbsPos(item.center);
                ret += " node[" + item.name + "]{}";
            }
            ret += "\n"
        }

        return ret;
    }
    getItemAtPos(pos){
        for(let idx = this.items.length-1; idx>=0; idx--){
            if(this.items[idx].isAtPos(pos)){
                return this.items[idx];
            }
        }
        return undefined;
    }
}