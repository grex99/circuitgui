class Grid{
    scale = 35;
    color = "#F0F0F0";
    center = new Vector(210,490);

    /**
     * draw the grid one the canvas 
     */
    draw(){
        gc.strokeStyle = this.color;
        gc.beginPath();
        for(let idx =this.scale; idx<1050;idx+=this.scale){
            gc.moveTo(idx,0);
            gc.lineTo(idx,700);
        }
        for(let idy =this.scale; idy<700;idy+=this.scale){
            gc.moveTo(0,idy);
            gc.lineTo(1050,idy);
        }   
        gc.stroke();

        gc.beginPath();
        gc.strokeStyle = "#034c3c";
        gc.moveTo(this.center.x-10,this.center.y);
        gc.lineTo(this.center.x+10,this.center.y);
        gc.moveTo(this.center.x,this.center.y+10);
        gc.lineTo(this.center.x,this.center.y-10);
        gc.stroke();
    }

    /**
     * snaps the values from a vector to the grid
     * @param {Vector} pos
     * @returns {Vector}  
     */
    snap(pos){
        pos.x = Math.round(pos.x/this.scale)*this.scale;
        pos.y = Math.round(pos.y/this.scale)*this.scale;
        return pos;
    }

    getAbsPos(pos){
        let vector = Vector.sub(pos,this.center);
        return "(" + vector.x/this.scale/2 + "," + -vector.y/this.scale/2 + ")";
    }
    getRelPos(pos){
        return "(" + pos.x/this.scale/2 + "," + -pos.y/this.scale/2 + ")";
    }


    
}