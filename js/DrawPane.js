class DrawPane{
    canvas;
    constructor(){
        this.canvas = document.getElementById("myCanvas");
        this.canvas.onmousemove = PlaceTool.onmousemove;
        this.canvas.onmousedown = PlaceTool.onmousedown;
        this.canvas.addEventListener("touchstart", PlaceTool.onmousedown, false);
        this.canvas.addEventListener("touchmove", PlaceTool.onmousemove, false);
        this.canvas.addEventListener("touchend", PlaceTool.onmousedown, false);
        this.canvas.onmouseleave = DrawPane.onmouseleave;
        this.canvas.grid = new Grid();
        this.canvas.itemHandler = new ItemHandler();
        this.canvas.update = function(){
            gc.fillStyle = "#FFFFFF";
            gc.fillRect(0,0,1050,700);
            this.grid.draw();
        
            gc.strokeStyle = "#000000";
            this.itemHandler.draw();
        }
        this.canvas.nextItem = new Item("short");
    }

    /**
     * snaps the values from a vector to the grid
     * @returns {CanvasRenderingContext2D}  
     */
    getContext(gc_ = this.canvas.getContext("2d")){
        gc_.connectVectors = function(vec1, vec2, ...others){
            this.beginPath();
                for(let idx = 0; idx<arguments.length; idx+=2){
                    this.moveTo(arguments[idx].x, arguments[idx].y);
                    this.lineTo(arguments[idx+1].x, arguments[idx+1].y);

                }
            this.stroke();
        }
        gc_.drawVector = function(vector, start){
            this.connectVectors(start, Vector.add(start, vector));
        }
        gc_.drawVectorArray = function(vectorArray, startPos){
            this.movePath(vectorArray, startPos);
            this.stroke();
        }
        gc_.fillVectorArray = function(vectorArray, startPos){
            this.movePath(vectorArray,startPos);
            this.fill();
        }
        gc_.movePath = function(vectorArray, startPos){
            let pos = startPos.copy();
            this.beginPath();
            this.moveTo(startPos.x, startPos.y);
            for(let idx = 0; idx<vectorArray.length; idx++){
                pos.add(vectorArray[idx]);
                this.lineTo(pos.x,pos.y);
            }
        }
        gc_.mark = function(pos, farbe){
            this.strokeStyle = farbe;
            gc.strokeRect(pos.x-5, pos.y-5, 10,10);
        }
        gc_.roundRect = function (x, y, w, h, r) {
            //if (w < 2 * r) r = w / 2;
            //if (h < 2 * r) r = h / 2;
            this.beginPath();
            this.moveTo(x+r, y);
            this.lineTo(x+w-r,y)
            this.arcTo(x+w, y,   x+w, y+h, r);
            this.lineTo(x+w,y+h-r);
            this.arcTo(x+w, y+h, x+w-r,   y+h, r);
            this.lineTo(x+r,y+h);
            this.arcTo(x,   y+h, x,   y+h-r,   r);
            this.lineTo(x,y+r);
            this.arcTo(x,   y,   x+r, y,   r);
            this.closePath();
          }
        return gc_;
    }

    /**
     * updates the canvas to draw all items
     */
    update(){
        this.canvas.update();
    }

    /**
     * set the function to call on mousedown
     * @param {Function} fun
     */
    onmousedown(func){
        this.canvas.onmousedown = func;
    }

    /**
     * set the function to call on mousemove
     * @param {Function} fun
     */
    onmousemove(func){
        this.canvas.onmousemove = func;
    }
    static onmouseleave(e){
        drawPane.update();
    }
}