//Wir testen heir die Matrixschreibweise. Der Vorwärtstransmissionsfaktor 
//Mission Miso 
class ItemTree{
    root;
    constructor(){
        this.root = new Node("root");

    }
    getNode(param){
        if(Array.isArray(param)){
            if(param.length == 0 && this.root.children.length == 0) return undefined;
            let node = this.root;
            for(let i in param){
                node = node.children[param[i]];
            }
            return node;
        }
    }
    getNodeFromValue(value){
        let iter = new NodeIterator(this);
        while(iter.hasNext()){
            let n = iter.getNext();
            if(n.value == value) return n;
        }
    }
    hasNode(param){
        if(Array.isArray(param)){
            if(param.length == 0 && this.root.children.length == 0) return false;
            let node = this.root;
            for(let i in param){
                node = node.children[param[i]];
                if(node == undefined){
                    return false;
                }
            }
            return true;
        }
    }
    add(parent, ...values){
        let p = this.root;
        let iter = new NodeIterator(this);
        while(iter.hasNext()){
            let node = iter.getNext();
            if(node.value == parent){
                p = node;
                break;
            }        
        }
        for(let i in values){
            new Node(values[i], p);
        }
    }
    getPath(value){
        let path = "";
        let iter = new NodeIterator(this);
        let node;
        while(iter.hasNext()){
            node = iter.getNext();
            if(node.value == value){
                path = "" + node.value;
                while(node.parent != undefined){
                    node = node.parent;
                    path = node.value + "," + path;
                }
                return path;
            }
        }
    }
    getChildren(val){
        if(val == "root"){
            let ret = [];
            for(let i in this.root.children){
                ret.push(this.root.children[i].value);
            }
            return ret;
        }

        let iter = new NodeIterator(this);
        while(iter.hasNext()){
            let node = iter.getNext();
            if(node.value == val){
                let ret = [];
                for(let i in node.children){
                    ret.push(node.children[i].value);
                }
                return ret;
            }
        }
    }
    getSiblings(val){
        let iter = new NodeIterator(this);
        let ret = [];
        while(iter.hasNext()){
            let node = iter.getNext();
            if(node.value == val){
                for(let i in node.parent.children){
                    if(node.parent.children[i].value != val){
                        ret.push(node.parent.children[i].value);
                    }
                }
                return ret;
            }
        }
        return ret;
    }
    print(){
        let iter = new NodeIterator(this);
        while(iter.hasNext()){
            let line = " ";
            let node = iter.getNext();
            let count = this.getPath(node.value).split(",").length-2;
            for(let i = 0; i<count; i++){
                line += "    ";
            }
            console.log(line, node.value);
        }
    }
}

class Node{
    value;
    parent;
    children = [];
    constructor(_value, _parent){
        this.value = _value;
        if(_parent != undefined){
            this.parent = _parent;
            this.parent.children.push(this);
        }
    }
}

class NodeIterator{
    tree;
    pos = [];
    constructor(_tree){
        this.tree = _tree;
    }
    hasNext(){
        let _pos = [];
        for(let i in this.pos){
            _pos[i] = this.pos[i];
        }
        while(true){
            _pos.push(0);
            if(this.tree.hasNode(_pos)){
                return true;
            }
            _pos.pop();
            if(_pos.length == 0) return false;

            _pos[_pos.length-1]++;
            if(this.tree.hasNode(_pos)){
                return true;
            }
            _pos.pop();
            if(_pos.length == 0) return false;

            _pos[_pos.length-1]++;
            if(this.tree.hasNode(_pos)){
                return true;
            }

            if(_pos.length == 1 && _pos[_pos.length-1] == this.tree.root.children.length){
                return false;
            }

        }
        return false;
    }
    getNext(){
        while(true){
            this.pos.push(0);
            if(this.tree.hasNode(this.pos)){
                return this.tree.getNode(this.pos);
            }
            this.pos.pop();
            if(this.pos.length == 0) return undefined;

            this.pos[this.pos.length-1]++;
            if(this.tree.hasNode(this.pos)){
                return this.tree.getNode(this.pos);
            }
            this.pos.pop();
            if(this.pos.length == 0) return undefined;
            
            this.pos[this.pos.length-1]++;
            if(this.tree.hasNode(this.pos)){
                return this.tree.getNode(this.pos);
            }

            if(this.pos.length == 1 && this.pos[this.pos.length-1] == this.tree.root.children.length){
                return undefined;
            }
        }
    }
    reset(){
        this.pos = [];
    }
}

class ValueIterator{
    iter;
    constructor(tree){
        this.iter = new NodeIterator(tree);
    }
    hasNext(){
        return this.iter.hasNext();
    }
    getNext(){
        return this.iter.getNext().value;
    }
}

class LeaveNodeIterator{
    tree;
    pos;
    constructor(_tree){
        this.tree = _tree;
        this.pos = [0];
    }
    hasNext(){
        let _pos = [];
        for(let i in this.pos){
            _pos[i] = this.pos[i];
        }

        while(_pos.length > 0){
            while(this.tree.hasNode(_pos)){
                _pos.push(0);
                if(this.tree.hasNode(_pos) == false){ 
                    _pos.pop();
                    _pos[_pos.length-1]++;
                    return true;
                }
            }


            _pos.pop();
            _pos[_pos.length-1]++;
        }
        return false;
    }
    getNext(){
        while(this.pos.length > 0){
            while(this.tree.hasNode(this.pos)){
                this.pos.push(0);
                if(this.tree.hasNode(this.pos) == false){ 
                    this.pos.pop();
                    let node = this.tree.getNode(this.pos);
                    this.pos[this.pos.length-1]++;
                    return node;
                }
            }


            this.pos.pop();
            this.pos[this.pos.length-1]++;
        }
        return undefined;
    }
    reset(){
        this.pos = [0];
    }
}