class Vector{
  constructor(x,y){
    this.x = x;
    this.y = y;
  }
  /**
   * create a copy of this vector
   * @returns {Vector}  
   */
  copy(){
    return new Vector(this.x, this.y);
  }

  /**
   * add vectors
   * @param {Vector} others
   * @returns {Vector}  
   */
  add (...others){
    if(arguments[0] == undefined) return this;
    let _x = 0;
    let _y = 0;
    for(let i in others){
      _x += others[i].x;
      _y += others[i].y;
    }
    this.x += _x;
    this.y += _y;
    return this;
  }

  /**
   * add vectors to a new Vector
   * @param {Vector} first
   * @param {Vector} others
   * @returns {Vector}  
   */
  static add(first, ...others){
    let ret = first.copy();
    for(let i in others){
      ret.add(others[i]);
    }
    return ret;
  }

  /**
   * subract vectors to a new Vector
   * @param {Vector} others
   * @returns {Vector}  
   */
  sub(...others){
    let _x = 0;
    let _y = 0;
    for(let i in others){
      _x -= others[i].x;
      _y -= others[i].y;
    }
    this.x += _x;
    this.y += _y;
    return this;
  }

  /**
   * subtract vectors to a new Vector
   * @param {Vector} first
   * @param {Vector} others
   * @returns {Vector}  
   */
  static sub(first, ...others){
    let ret = first.copy();
    for(let i in others){
      ret.sub(others[i]);
    }
    return ret;
  }

  /**
   * multiply with a number
   * @param {Number} a
   * @returns {Vector}  
   */
  mult(a){
    this.x *= a;
    this.y *= a;
    return this;
  }

  /**
   * divide by a number
   * @param {Number} a
   * @returns {Vector}  
   */
  div(a){
    this.x /= a;
    this.y /= a;
    return this;
  }
  /**
   * get the length of this vector
   * @returns {Vector}  
   */
  abs(){
    return Math.sqrt(this.x * this.x + this.y * this.y);
  }

  /**
   * returns the distance to an other point
   * @returns {Vector}  
   */
  dist(other){
    return Vector.sub(this,other).abs();
  }
  /**
   * check if both vectors are equal to each other
   * @param {Vector} other
   * @returns {Boolean}  
   */
  equals(other){
    if(other == undefined) return false;
    return (other.x == this.x && other.y == this.y);
  }

  /**
   * set length to 1
   * @returns {Vector}  
   */
  normalize(){
    this.div(this.abs());
    return this;
  }

  setAbs(val){
    this.normalize();
    this.mult(val);
    return this;
  }
  
  /**
   * calculate the dot product
   * @param {Vector} other
   * @returns {Number}  
   */
  dot(other){
    return (this.x * other.x + this.y * other.y);
  }

  /**
   * returns the quadrant in wich the vector points
   * @returns {Number}  
   */
  getQ(){
    if(this.x >=0 && this.y >= 0) return 1;
    else if (this.x < 0 && this.y >= 0) return 2;
    else if (this.x < 0 && this.y < 0) return 3;
    else return 4;
  }

  /**
   * returns angle to the x-axis in rad
   * @returns {Number}  
   */
  angle(){
    if (this.x == 0 && this.y == 0) return 0;
    else if (this.x == 0 && this.y >= 0) return Math.PI/2;
    else if (this.x == 0 && this.y == 0) return -Math.PI/2;
    
    let ret = Math.atan(this.y / this.x);
    if (this.getQ() == 2) ret += Math.PI;
    if (this.getQ() == 3) ret -= Math.PI;
    return ret;
  }
  
  /**
   * returns angle to the x-axis in degrees
   * @returns {Number}  
   */
  angleDeg(){
    return this.angle()*180/Math.PI;
  }

  /**
   * returns the angle to an other vector
   * @param {Vector} other
   * @returns {Number}  
   */
  angleTo(other){
    let ret = other.angle() - this.angle();
    if(ret < -Math.PI) ret += 2*Math.PI;
    else if(ret > Math.PI) ret -= 2*Math.PI;
    return ret;
  }
  /**
   * returns the angle to an other vector in degrees
   * @param {Vector} other
   * @returns {Number}  
   */
  angleToDeg(other){
    return this.angleTo(other)*180/Math.PI;
  }
  /**
   * rotate this vector, phi in rad
   * @param {Number} phi
   * @returns {Vector}  
   */
  rotate(phi){
    let _x = this.x * Math.cos(phi) - this.y * Math.sin(phi);
    let _y = this.x * Math.sin(phi) + this.y * Math.cos(phi);
    this.x = _x;
    this.y = _y;
    return this;
  }
  /**
   * rotate this vector, phi in degrees
   * @param {Number} phi
   * @returns {Vector}  
   */
  rotateDeg(phi){
    this.rotate(phi*Math.PI/180);
    return this;
  }

  /**
   * set its own size to the scalar projection from other
   * @param {Vector} other
   * @returns {Vector}  
   */
  project(other){
    this.normalize();
    this.mult( other.dot(base) );
    return this;
  }

  /**
   * returns the length of a scalar projection, does not change the vectors
   * @param {Vector} other
   * @returns {Vector}  
   */
  projectLength(other){
    let base = this.copy().normalize();
    return other.dot(base);
  }
  
  /**
   * returns true if the projection is shorter than its own length
   * @param {Vector} other
   * @returns {Boolean}  
   */
  projectedInLength(other){
    let ret = this.projectLength(other);
    return (ret >= 0 && ret <= this.abs());
  }

  /**
   * creates array of vectors from all arguments: (1,1, 2,2, 3,3)-> [(1,1), (2,2), (3,3)] 
   * @param {Vector} ...others
   * @returns {Array}  
   */
  static newArray(...others){
    let ret = [];
    for(let idx = 0; idx < arguments.length; idx+=2){
      ret.push(new Vector(arguments[idx], arguments[idx+1]));
    }
    return ret;
  }
  
  /**
   * rotate every vector in an array for phi rad
   * @param {Array} array
   * @param {Number} phi
   * @returns {Array}  
   */
  static rotateArray(array, phi){
      for(let idx = 0; idx<array.length; idx++){
        array[idx].rotate(phi);
      }
      return array;
  }

  /**
   * rotate every vector in an array for phi degrees
   * @param {Array} array
   * @param {Number} phi
   * @returns {Array}  
   */
  static rotateArrayDeg(array, phi){
      for(let idx = 0; idx<array.length; idx++){
        array[idx].rotateDeg(phi);
      }
      return array;
  }
}
