class EraseTool{
    
    static onmousedown(e){
        let pos = new Vector(e.pageX-this.offsetLeft, e.pageY-this.offsetTop);
        let item = drawPane.canvas.itemHandler.getItemAtPos(pos);
        if(item != undefined){
            drawPane.canvas.itemHandler.remove(item);
            drawPane.update();
            textpane.update();
        }
    }
    
    static onmousemove(e){
        let pos = new Vector(e.pageX-this.offsetLeft, e.pageY-this.offsetTop);
        drawPane.update();
        let item = drawPane.canvas.itemHandler.getItemAtPos(pos);
        if(item != undefined){
            item.draw("#FF0000");
        }
    }
}

let eraseToolButton = document.getElementById("eraseToolButt");
eraseToolButton.addEventListener("click", (e) => {
    drawPane.onmousemove(EraseTool.onmousemove);
    drawPane.onmousedown(EraseTool.onmousedown);
})