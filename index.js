"use strict"

let pressedKeys = [];
document.addEventListener("keydown", (e)=>{
    pressedKeys.push(e.key)
})
document.addEventListener("keyup", (e)=>{
    pressedKeys.splice(pressedKeys.indexOf(e.key))
})

let itemtree = new ItemTree();


itemtree.add("root", "bipoles", "monopoles");
itemtree.add("bipoles", "primitives", "resistors", "capacitors", "inductors", "diodes", "sources");
itemtree.add("primitives", "short");

itemtree.add("resistors", "european resistor");
itemtree.add("capacitors", "capacitor", "ecapacitor");
itemtree.add("inductors", "european inductor", "american inductor");
itemtree.add("diodes", "stroke diode");
itemtree.add("sources", "european voltage source", "european current source", "esource");

itemtree.add("monopoles", "grounds", "shapes");
itemtree.add("grounds", "ground", "tlground", "rground");
itemtree.add("shapes", "circ", "ocirc");

let drawPane = new DrawPane();
let gc = drawPane.getContext();
drawPane.update();

let buttonpane = new ButtonPane();
buttonpane.createButtonFromName(["ground","short","circ","european resistor", "capacitor", "american inductor", "european voltage source", "stroke diode"]);

let textpane = new TextPane();
